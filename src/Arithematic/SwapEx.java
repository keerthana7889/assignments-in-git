package Arithematic;

import java.util.Scanner;

public class SwapEx {

	public static void main(String[] args) {
		int a , b;
		
		System.out.println("Enter the value of a and b");
		Scanner sc=new Scanner(System.in);
		a=sc.nextInt();
		b=sc.nextInt();
		
		System.out.println("before swapping:"+a + " " +b );
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swapping:" +a +" " +b);
		sc.close();
		
 
	}

}
